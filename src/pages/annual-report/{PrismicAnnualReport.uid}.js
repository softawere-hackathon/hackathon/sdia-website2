import React from 'react';
import { graphql } from 'gatsby';
import { withPrismicPreview } from 'gatsby-plugin-prismic-previews';

import { Layout } from '/src/components/Layout';
import SliceZone from '/src/components/SliceZone';
import Seo from '/src/components/Seo';
import { RichText } from 'prismic-reactjs'

const AnnualReportTemplate = ({ data }) => {
  if (!data) return null;

  const { body } = data.prismicAnnualReport?.data || {};
  const doc = data.prismicAnnualReport?.data;
  const {
    page_title,
    page_description,
    share_image
  } = doc;

  let pageDescription = null;
  let pageTitle = null;
  let shareImage = share_image;

  if (page_description && Array.isArray(page_description.richText) && page_description.richText[0]?.text?.length > 0) {
    pageDescription = RichText.asText(page_description.richText);
  }

  if (page_title && Array.isArray(page_title.richText) && page_title.richText[0]?.text?.length > 0) {
    pageTitle = RichText.asText(page_title.richText);
  }

  if (share_image) shareImage = share_image;

  return (
    <Layout
      useDarkFooter={true}
      useDarkHeader={false}
    >
      <Seo
        description={pageDescription}
        image={shareImage}
        title={pageTitle || 'Annual Report'}
      />
      <div className="bg-white">
        {body && <SliceZone sliceZone={body} />}
      </div>
    </Layout>
  );
};

export const query = graphql`
  query AnnualReport {
    prismicAnnualReport {
      _previewable
      data {
        page_description {
          richText
        }
        page_title {
          richText
        }
        share_image {
          url
        }
        body {
          ...on PrismicSliceType {
            slice_type
          }
          ...AnnualReportDataBodyTwoTeamMembersWithText
          ...AnnualReportDataBodyAnnualReportHero
          ...AnnualReportDataBodyCardsWithImages
          ...AnnualReportDataBodyTextWithImage
          ...AnnualReportDataBodySlideshowWithBullets
          ...AnnualReportDataBodyAnnualReportSubjects
          ...AnnualReportDataBodyVideoTestimonials
          ...AnnualReportDataBodyQuoteWithCallToAction
          ...AnnualReportDataBodyTextWithCallToActionsAndBackgroundImage
          ...AnnualReportDataBodyVerticallyCenteredImage
          ...AnnualReportDataBodySteeringGroups
        }
      }
    }
  }
`;

export default withPrismicPreview(AnnualReportTemplate);
