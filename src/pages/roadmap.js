import * as React from 'react'
import { graphql } from 'gatsby'
import { withPrismicPreview } from 'gatsby-plugin-prismic-previews'
import { RichText } from 'prismic-reactjs'
import ScrollContainer from 'react-indiana-drag-scroll'
import BodyClassName from 'react-body-classname'
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import {
  addYears,
  eachQuarterOfInterval,
  intervalToDuration,
  getYear,
} from 'date-fns'
import { times } from 'lodash'

import { Layout } from '../components/Layout'
import LinkButton from '../components/LinkButton'
import Seo from '../components/Seo'
import Quarters from '../components/roadmap/Quarters'
import MetricWithActions from '../components/roadmap/MetricWithActions'
import ListActivities from '../components/roadmap/ListActivities'

const roadmapStartDate = new Date(2021, 0, 1) // 1st of January, 2021
const roadmapEndDate = new Date(2030, 11, 31) // 31st of January, 2030
const roadmapInterval = { start: roadmapStartDate, end: roadmapEndDate }
const roadmapAllQuarters = eachQuarterOfInterval(roadmapInterval)
const roadmapYearsCount = intervalToDuration(roadmapInterval).years + 1
const roadmapYears = times(roadmapYearsCount, (n) => {
  return getYear(addYears(roadmapStartDate, n))
})

const RoadmapPage = ({ data }) => {
  if (!data) return null
  const doc = data.prismicRoadmapPage.data
  const bgImage = getImage(doc.background_image)
  const {
    title,
    summary,
    call_to_actions,
    metrics,
  } = doc

  return (
    <Layout useDarkHeader={false} useDarkFooter={false}>
      <BodyClassName className="c-roadmap-page" />
      <Seo title={RichText.asText(title.richText)} description={RichText.asText(summary.richText)} />

      <GatsbyImage 
        image={bgImage} 
        alt={RichText.asText(title.richText)} 
        className="c-roadmap-earth fixed right-0 top-0 z-10 block" 
      />

      <div className="container-medium c-roadmap-page-header pt-48">
        <h1 className="text-white">{RichText.asText(title.richText)}</h1>
        <p className="text-white text-lg mt-4 mb-6 font-light leading-relaxed">{RichText.asText(summary.richText)}</p>

        {call_to_actions.map((action, index) => (
          <LinkButton link={action.link} label={action.label} isWarning key={`roadmap-action-${index}`} className="mr-4" />
        ))}
      </div>

      <div className="c-roadmap-table-wrapper hidden lg:block">
        <ScrollContainer
          className="scroll-container c-roadmap-table-wrapper-scrollcontainer u-relative"
          vertical={false}
          horizontal={true}
        >
          <table cellPadding="0" cellSpacing="0" border="0" className="c-roadmap-table">
              <thead>
                <tr>
                  <th className="c-roadmap__top-title" colSpan="2" />
        
                  {roadmapYears.map(year => (
                    <th key={year.toString()} className="c-roadmap__top-title" colSpan="4">{year}</th>
                  ))}
                </tr>
                  
                <Quarters years={roadmapYears} buffer={2} />
              </thead>
              <tbody>
                {metrics.map((item) => (
                  <MetricWithActions 
                    key={item.metric.document.id}
                    uid={item.metric.document.uid}
                    roadmapStartDate={roadmapStartDate}
                    roadmapAllQuarters={roadmapAllQuarters}
                    {...item.metric.document.data}
                  />
                ))}
              </tbody>
            </table>
        </ScrollContainer>
      </div>

      <div className="container-medium bg-white rounded-lg lg:hidden z-20 relative py-8 px-8 mt-12">
        <p className="italic text-lg">
          <strong>For the best experience:&nbsp;</strong> 
          To view our roadmap as a timeline, please switch to a larger screen or increase the size of your browser window.
        </p>

        <h2 className="mt-8 mb-8">Our roadmap priorities:</h2>

        <ListActivities metrics={metrics.map(item => item.metric.document)} />
      </div>
    </Layout>
  )
}

export const query = graphql`
  query RoadmapPage {
    prismicRoadmapPage {
      _previewable
      data {
        background_image {
          gatsbyImageData(width: 1600, imgixParams: {q: 40})
        }
        call_to_actions {
          label {
            richText
          }
          link {
            url
            type
            target
            link_type
          }
        }
        summary {
          richText
        }
        title {
          richText
        }
        activity_target_label
        contributing_members_label
        impact_principle_label
        sd_goal_label
        metrics {
          metric {
            document {
              ...PrismicRoadmapMetricFragment
            }
          }
        }
      }
    }
  }

`

export default withPrismicPreview(RoadmapPage)
