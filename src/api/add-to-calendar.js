import { addHours, addMinutes } from 'date-fns'
import { padStart } from 'lodash'
// Allowed parameters

// calendar = google, outlook, ics
// eventUrl
// year
// month
// day
// hourOfDay
// minuteOfDay
// durationHours
// durationMinutes
// title
// description
// location
// geoLat, geoLon
// timeZone = Europe/Berlin

export default function handler(req, res) {

  const {
    calendar,
    eventUrl,
    year,
    month,
    day,
    hourOfDay,
    minuteOfDay,
    durationHours,
    durationMinutes,
    title,
    description,
    location,
    geoLat,
    geoLon,
    timeZone,
  } = req.query

  const { zonedTimeToUtc, formatInTimeZone, toDate } = require('date-fns-tz')

  const dateString = `${year}-${padStart(month, 2, '0')}-${padStart(day, 2, '0')} ${padStart(hourOfDay, 2, '0')}:${padStart(minuteOfDay, 2, '0')}:00`
  const date = toDate(dateString, { timeZone: (timeZone || 'Europe/Berlin') })

  switch (calendar) {
    case 'google':
      const uri = new URL('https://www.google.com/calendar/render')
      const pattern = "yyyyMMdd'T'HHmmss'Z'"
      const start = formatInTimeZone(date, 'UTC', pattern)
      const endDate = addMinutes(addHours(date, durationHours), durationMinutes)
      const end = formatInTimeZone(endDate, 'UTC', pattern)

      const params = new URLSearchParams({
        action: 'TEMPLATE',
        text: title,
        details: description,
        location: location,
        dates: `${start}/${end}`
      })
      uri.search = params.toString()

      // res.status(200).send(uri.toString())
      return res.redirect(302, uri.toString())
    case 'outlook':
      const outlookUri = new URL('https://outlook.office.com/calendar/0/deeplink/compose')
      const outlookPattern = "yyyy-MM-dd'T'HH:mm:ss:SS"
      const outlookStart = formatInTimeZone(date, 'UTC', outlookPattern)
      const outlookEndDate = addMinutes(addHours(date, durationHours), durationMinutes)
      const outlookEnd = formatInTimeZone(outlookEndDate, 'UTC', outlookPattern)

      const outlookParams = new URLSearchParams({
        subject: title,
        body: description,
        location: location,
        enddt: outlookEnd,
        startdt: outlookStart,
        path: 'calendar/action/compose',
        rru: 'addevent'
      })
      outlookUri.search = outlookParams.toString()

      return res.redirect(302, outlookUri.toString())
    case 'ics':
      const ics = require('ics')

      const event = {
        start: [parseInt(year), parseInt(month), parseInt(day), parseInt(hourOfDay), parseInt(minuteOfDay)],
        duration: { hours: parseInt(durationHours), minutes: parseInt(durationMinutes) },
        title: title,
        description: description,
        location: location,
        url: eventUrl,
        geo: { lat: geoLat, lon: geoLon },
        status: 'CONFIRMED',
        busyStatus: 'BUSY',
        // organizer: { name: organizerName, email: organizerEmail },
        alarms: [
          { action: 'display', description: 'Reminder', trigger: { hours: 0, minutes: 15, before: true } },
          { action: 'display', description: 'Reminder', trigger: { hours: 0, minutes: 0, before: true } },
        ]
      }

      ics.createEvent(event, (error, value) => {
        if (error) {
          res.status(403).send(error)
          return
        }

        res.setHeader('Content-Type', 'text/calendar; charset=utf-8')
        res.setHeader('Content-Disposition', 'inline; filename=calendar.ics')

        res
          .status(200)
          .send(value)

        return
      })
    default:
      res.status(406).send('Calendar type not supported')
      return
  }
}
