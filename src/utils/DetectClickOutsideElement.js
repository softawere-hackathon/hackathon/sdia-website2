const detectClickOutsideElement = (event, elementRef) => {
  if (!event || !elementRef) return false;

  const { target } = event;

  return elementRef !== target && !elementRef.contains(target);
};

export default detectClickOutsideElement;
