import * as React from 'react'
import { RichText } from 'prismic-reactjs'

// import LinkButton from './LinkButton'
import Podcast from './Podcast'

const ListPodcasts = ({ allPodcasts, title, subtitle }) => {
  return (
    <section className={`mt-12 sm:mt-20 lg:pb-8 container`}>
      {title ? 
        <h2 className="text-center">
          {RichText.asText(title.richText)}
        </h2> : null}

      {subtitle ? 
        <p className="text-xl font-light font-display text-gray-500 mt-3 text-center">
          {RichText.asText(subtitle.richText)}
        </p> : null}

      <div className="grid sm:grid-cols-2 lg:grid-cols-3 gap-4 mt-12">
        {allPodcasts.map((podcast, index) => (
          <Podcast {...podcast} key={`podcast-${index}`} />
        ))}
      </div>
    </section>
  )
}

export default ListPodcasts
