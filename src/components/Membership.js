import { graphql } from "gatsby";

export const membershipFragment = graphql`
  fragment PrismicMembershipFragment on PrismicMembership {
    id
    _previewable
    data {
      call_to_action {
        url
        uid
        type
        target
        link_type
      }
      price
      currency
      call_to_action_label
      color
      features {
        text {
          richText
        }
      }
      name {
        richText
      }
      tagline {
        richText
      }
    }
  }
`