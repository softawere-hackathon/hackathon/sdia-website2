import * as React from 'react'
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import { RichText } from 'prismic-reactjs'

const SdGoal = ({ icon, name, ...props }) => (
  <GatsbyImage
    alt={RichText.asText(name.richText)}
    image={getImage(icon)}
    className="rounded shadow-lg mr-4"
  />
)

export default SdGoal


                  