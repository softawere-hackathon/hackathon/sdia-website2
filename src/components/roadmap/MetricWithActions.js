import React from 'react'
import Action from './Action'
import {
  eachQuarterOfInterval,
  parse,
  differenceInCalendarQuarters,
  isBefore,
  isEqual,
} from 'date-fns'
import { RichText } from 'prismic-reactjs'

const MetricWithActions = ({ 
  activities, 
  current_measure, 
  explanation, 
  name, 
  uid,
  roadmapStartDate,
  roadmapAllQuarters,
  ...props 
}) => {
  let actions = activities.filter((item) => item && item.activity && item.activity.document).map((item) => {
    const {
      end_date,
      start_date,
      name,
    } = item.activity.document.data
    
    const actionInterval = { 
      start: parse(start_date, 'yyyy-MM-dd', new Date()),
      end: parse(end_date, 'yyyy-MM-dd', new Date()) 
    }

    return {
      id: item.activity.document.id, 
      url: item.activity.document.url,
      quarters: eachQuarterOfInterval(actionInterval).length,
      start_quarter: differenceInCalendarQuarters(actionInterval.start, roadmapStartDate),
      total_quarters: roadmapAllQuarters.length,
      start_date: actionInterval.start,
      end_date: actionInterval.end,
      name,
      metric_uid: uid,
    }
  })

  actions.sort((a, b) => {
    const startDateA = a.start_date
    const startDateB = b.start_date

    if(isEqual(startDateA, startDateB)) {
      return 0
    } else if(isBefore(startDateA, startDateB)) {
      return -1
    } else {
      return 1
    }
  })

  return (
    <>
      <tr>
        <td rowSpan={activities.length + 1} className="c-roadmap-title-left w-full">
          <span className="text-lg">{name}</span>

          <div className="c-roadmap-title-left__sub inline-flex items-center">
            <span className="text-sm leading-snug">{current_measure}</span>
            {explanation ?
              <div className="relative group ml-2">
                <svg className="w-7 h-7 fill-current text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 256">
                  <path d="M128.00146,24a104,104,0,1,0,104,104A104.12041,104.12041,0,0,0,128.00146,24Zm-.001,168a12,12,0,1,1,12-12A12,12,0,0,1,128.00049,192Zm28.28222-51.71875a39.6414,39.6414,0,0,1-20.28125,10.92187V152a8,8,0,1,1-16,0v-8a7.9954,7.9954,0,0,1,8-8,23.99744,23.99744,0,0,0,16.96875-40.96875c-9.0625-9.07813-24.88281-9.07813-33.92968,0A23.78173,23.78173,0,0,0,104.00146,112a8,8,0,0,1-16,0A39.73412,39.73412,0,0,1,99.72021,83.71094c15.09375-15.09375,41.46094-15.10157,56.57032.00781a39.99485,39.99485,0,0,1-.00782,56.5625Z"/>
                </svg>
                <div className="w-80 transform -translate-y-8 group-hover:-translate-y-8 opacity-0 focus:opacity-100 text-base -mr-6 -mt-6 z-40 group-hover:opacity-100 transition absolute bottom-0 left-0 bg-white shadow p-4 rounded text-gray-900">
                  <p>{RichText.asText(explanation.richText)}</p>
                </div>
              </div>
            : null }
          </div>
        </td>
      </tr>
      {actions.map((action, index) => (
        <Action 
          first={index === 0}
          last={index === (actions.length - 1)}
          key={`${action.id}-action-${index}`} 
          {...action} 
          {...props}
        />
      ))}
    </>  
  )
}

export default MetricWithActions