import React, { useRef } from 'react'
import Modal from '../Modal'
import { RichText } from 'prismic-reactjs'
import { currentStateStyling } from './Challenge'
import { remove } from 'lodash'

const ChallengeDetail = ({
  title,
  current_state,
  regional_scope,
  category,
  criticality,
  all_indicators,
}) => {
  // clean up indicators
  if(Array.isArray(all_indicators)) {
    remove(all_indicators, item =>
      RichText.asText(item.indicator.richText) === ''
    )
  }

  const modalRef = useRef()
  const modalFooter = (
    <>
      <button
        type="button"
        class="c-button c-button--warning"
        onClick={() => modalRef.current.closeModal()} >
          Close
      </button>
    </>
  )

  return (
    <>
      {/* {current_state === 'Raising awareness' ||
      current_state === 'Work in progress' ||
      current_state === 'Lacking support' ?
        <button
          onClick={() => modalRef.current.openModal()}
          className='c-button c-button--primary mt-4 c-button--full text-sm leading-none'
        >
          Open challenge
        </button>
      : null} */}

      <Modal ref={modalRef} footer={modalFooter} title={RichText.asText(title.richText)}>
        <div className='inline-flex flex-wrap gap-y-2'>
          {current_state ?
            <span
              className={`inline text-sm font-medium mr-2 px-2.5 py-0.5 rounded ${currentStateStyling[current_state]}`}
            >
              <strong>State</strong>&nbsp;
              {current_state}
            </span>
          : null}

          {regional_scope ?
            <span
              className={`
                inline text-sm font-medium mr-2 px-2.5 py-0.5 rounded bg-white text-green-800
                border border-green-800
              `}
            >
              <strong>Regional Scope</strong>&nbsp;
              {regional_scope}
            </span>
          : null}

          {category ?
            <span
              className={`
                inline text-sm font-medium mr-2 px-2.5 py-0.5 rounded bg-white text-gray-600
                border border-gray-800
              `}
            >
              <strong>Category</strong>&nbsp;
              {category}
            </span>
          : null}

          {criticality ?
            <span
              className={`
                inline text-sm font-medium mr-2 px-2.5 py-0.5 rounded bg-white text-secondary-800
                border border-secondary-800
              `}
            >
              <strong>Criticality</strong>&nbsp;
              {criticality}
            </span>
          : null}
        </div>

        {all_indicators.length > 0 ?
          <section>
            <h5>Indicators</h5>
            <p className='text-sm text-gray-600'>These help us verify that this challenge can be observed.</p>
          </section>
        : null}
      </Modal>
    </>
  )
}

export default ChallengeDetail
