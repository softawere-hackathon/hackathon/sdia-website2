import { graphql } from 'gatsby'

export const individualMemberFragment = graphql`
  fragment PrismicIndividualMemberFragment on PrismicIndividualMember {
    _previewable
    uid

    data {
      title {
        richText
      }
      full_name {
        richText
      }
      linkedin_profile {
        url
        uid
        type
        target
        link_type
      }
      biography {
        richText
      }
      photo {
        gatsbyImageData(
          width: 130, height: 130, imgixParams: {
            fit: "facearea",
            facepad: 3.5
            maxH: 130
          }
        )
      }
    }
  }
`