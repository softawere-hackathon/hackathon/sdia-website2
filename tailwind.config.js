module.exports = {
  mode: 'jit',
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  important: true,
  corePlugins: {
    container: false
  },
  theme: {
    extend: {
      fontWeight: {
        normal: 400,
        light: 300,
        semibold: 500,
        bold: 700
      },
      fontFamily: {
        display: '"Roboto Condensed", Roboto, "Helvetica Neue", Arial, sans-serif',
        body: 'Roboto, "Helvetica Neue", Arial, sans-serif',
      },
      boxShadow: {
        '2': '0px 0px 50px -1px rgba(0, 0, 0, 0.1)',
      },
      fontSize: {
        'xxs': ['.5rem', '1rem'],
        'xs': ['.75rem', '1.25rem'],
        'sm': ['.875rem', '1.5rem'],
        'tiny': ['.875rem', '2rem'],
        'base': ['18px', '1.2'],
        'lg': ['1.125rem', '1.5rem'],
        'xl': ['1.35rem', '2rem'],
        '2xl': ['1.7rem', '2.2rem'],
        '3xl': ['2rem', '2.5rem'],
        '4xl': ['2.25rem', '2.75rem'],
        '4-5xl': ['2.5rem', '3rem'],
        '5xl': ['3rem', '4rem'],
      },
      lineHeight: {
        'tight': '1',
        'snug': '1.15',
        'normal': '1.25',
        'relaxed': '1.5',
        'loose': '2',
        '10': '2.85rem',
      },
      padding: {
        '144': '144px'
      },
      margin: {
        '144': '144px',
      },
      colors: {
        primary: {
          900: '#2A6049',
          800: '#487662',
          700: '#619174',
          600: '#91B29E',
          500: '#AFC7B9',
          400: '#C0D3C7',
          300: '#D0DED6',
          200: '#DFE9E3',
          100: '#F0F4F2'
        },
        secondary: {
          900: '#F48D2B',
          800: '#F6A455',
          700: '#F8B06B',
          600: '#F8BB80',
          500: '#F9C594',
          400: '#FBD1AA',
          300: '#FCDDC0',
          200: '#FDE8D5',
          100: '#FEF4EA'
        },
        gray: {
          900: '#1A202C',
          800: '#2D3748',
          700: '#4A5568',
          600: '#718096',
          500: '#A0AEC0',
          400: '#CBD5E0',
          300: '#E2E8F0',
          200: '#EDF2F7',
          100: '#F7FAFC',
        }
      },
      scale: {
        '99': '.99'
      },
      borderRadius: {
        DEFAULT: '5px'
      },
      transitionDuration: {
        '5000': '5000ms'
      },
      typography: (theme) => ({
        DEFAULT: {
          css: [
            {
              strong: {
                fontWeight: '700',
                color: 'inherit',
              },
              a: {
                color: theme('colors.secondary.900'),
              },
              h2: {
                marginTop: '1.5em',
                marginBottom: '0.75em'
              }
            },
            {
              fontSize: '18px',
              lineHeight: '1.35',
            }
          ]
        },
        lg: {
          css: [
            {},
            {
              fontSize: '1.15rem',
              lineHeight: '1.6rem',
            }
          ]
        },
        xl: {
          css: [
            {},
            {
              fontSize: '1.3rem',
              lineHeight: '1.9rem',
            }
          ]
        }
      })
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    // require('@tailwindcss/forms'),
    'gatsby-plugin-postcss',
  ],
}
