export async function onRequest(context) {
  const { env } = context;

  const url = "https://api.hubapi.com/marketing/v3/forms?limit=100"

  const headers = {
    "accept": "application/json",
    "authorization": `Bearer ${env.HUBSPOT_PRIVATE_APP_TOKEN}`
  }

  try {
    const result = await fetch(url, {
      method: "GET",
      headers: headers,
    });

    const response = JSON.stringify(await result.json());

    return new Response(response);
  } catch (error) {
    return new Response(error)
  }
}
