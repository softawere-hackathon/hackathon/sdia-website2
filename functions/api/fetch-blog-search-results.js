import * as prismic from '@prismicio/client';

const routes = [
  {
    type: 'blog',
    path: '/:uid',
  },
];

export async function onRequest(context) {
  const { env, request } = context;
  const data = await request.json();

  const { terms, page = 1 } = data;

  const accessToken = env.PRISMIC_ACCESS_TOKEN;
  const repoName = env.GATSBY_PRISMIC_REPO_NAME;
  const endpoint = prismic.getRepositoryEndpoint(repoName);
  const client = prismic.createClient(endpoint, { routes, fetch, accessToken });

  let results = [];

  if (!terms || typeof terms !== 'string') return results;

  results = await client.getByType('article', {
    graphQuery: `
      {
        article {
          author {
            full_name
            photo
          }
          categories {
            category {
              title
            }
          }
          excerpt
          main_image
          publication_time
          title
        }
      }
    `,
    orderings: {
      field: 'document.first_publication_date',
      direction: 'desc',
    },
    page,
    pageSize: 10,
    predicates: [prismic.predicate.fulltext('document', terms)]
  });

  const response = JSON.stringify(results);

  return new Response(response);
};
