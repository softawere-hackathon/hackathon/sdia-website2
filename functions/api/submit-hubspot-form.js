export async function onRequest(context) {
  const { env, request } = context;
  const data = await request.json();

  const { formId, formData } = data || {};

  const url = `https://api.hsforms.com/submissions/v3/integration/secure/submit/${env.GATSBY_HUBSPOT_PORTAL_ID}/${formId}`

  const headers = {
    "Authorization": `Bearer ${env.HUBSPOT_PRIVATE_APP_TOKEN}`,
    "Content-Type": "application/json"
  }

  try {
    const result = await fetch(url, {
      method: "POST",
      body: JSON.stringify(formData),
      headers: headers,
    })

    const response = JSON.stringify(await result.json());

    return new Response(response);
  } catch (error) {
    return new Response(error)
  }
}
