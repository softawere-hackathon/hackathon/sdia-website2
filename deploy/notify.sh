#/bin/bash
DEPLOYMENT_DATE=$(date -u +"%Y-%m-%dT%H:%M:%SZ") 
if [ "$CI_JOB_STATUS" == "success" ]; then
  EMOJI_STATUS=":white_check_mark:"
else
  EMOJI_STATUS=":exclamation:"
fi

curl -X POST -H 'Content-type: application/json' --data "{
  \"text\": \"*Project:* $CI_PROJECT_NAME\n*User:* $GITLAB_USER_NAME\n*Deployment Date:* $DEPLOYMENT_DATE\n*Status:* $CI_JOB_STATUS $EMOJI_STATUS\n*Details:* <$CI_JOB_URL|Job Details>\",
  \"mrkdwn\": true
}" $SLACK_WEBHOOK;