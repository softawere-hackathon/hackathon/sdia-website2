require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

// SEO & Social Media header data
const website = require('./config/website')

module.exports = {
  siteMetadata: {
    url: website.url + website.pathPrefix,
    siteUrl: website.url + website.pathPrefix, // For gatsby-plugin-sitemap
    pathPrefix: website.pathPrefix,
    image: website.sharingImage,
    ogLanguage: website.ogLanguage,
    author: website.author,
    twitterUsername: website.twitterUsername,
    titleTemplate: website.titleTemplate,
    description: website.description,
    title: website.title,
  },
  plugins: [
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /icons/,
          omitKeys: ['xmlnsDc', 'xmlnsCc', 'xmlnsRdf', 'xmlnsSvg', 'xmlnsSodipodi', 'xmlnsInkscape', 'style']
        }
      }
    },
    {
      resolve: `gatsby-plugin-schema-snapshot`,
      options: {
        path: `schema.gql`,
        exclude: {
          plugins: [`gatsby-source-npm-package-search`],
        },
        update: process.env.GATSBY_UPDATE_SCHEMA_SNAPSHOT,
      },
    },
    "gatsby-plugin-image",
    'gatsby-plugin-postcss',
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sitemap",
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        icon: "src/images/icon.png",
      },
    },
    {
      resolve: `gatsby-plugin-sass`,
      options: {
        postCssPlugins: [
          require("tailwindcss"),
          require("./tailwind.config.js"), // Optional: Load custom Tailwind CSS configuration
        ],
      },
    },
    {
      resolve: 'gatsby-source-prismic',
      options: {
        repositoryName: 'sdiav2',
        accessToken: 'MC5ZVVRFQVJJQUFDTUFoeGpP.77-9QlFacypK77-9YyXvv73vv73vv70TAxHvv73vv70uKn_vv73vv73vv70cDu-_vSXvv71ZQ--_vQ',
        customTypesApiToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoibWFjaGluZTJtYWNoaW5lIiwiZGJpZCI6InNkaWF2Mi1mNDFiNWE4NC0zMDZlLTRhZjYtOTMwYi0yN2YyZjNjNzRkMjFfNCIsImRhdGUiOjE2MzE4OTgzNjgsImRvbWFpbiI6InNkaWF2MiIsImlhdCI6MTYzMTg5ODM2OH0.culHsWd48FFKkvXc0zEpJDh8fNtUwakyZt2IIvWhN1M',
        linkResolver: require('./src/utils/LinkResolver').linkResolver,
      },
    },
    {
      resolve: 'gatsby-plugin-prismic-previews',
      options: {
        repositoryName: 'sdiav2',
      },
    },
    {
      resolve: 'gatsby-plugin-google-fonts',
      options: {
        fonts: [`Roboto\:300,400,500,700`, `Roboto Condensed\:300,400,500,700`],
      },
      display: 'swap'
    },
    {
      resolve: "gatsby-plugin-hubspot",
      options: {
        trackingCode: "7577247",
        respectDNT: true,
        productionOnly: true,
      },
    },
    {
      resolve: 'gatsby-plugin-load-script',
      options: {
        src: 'https://cdn.addevent.com/libs/atc/1.6.1/atc.min.js',
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        // Plugins configs
        plugins: [],
      },
    },
    {
      resolve: `gatsby-plugin-linkedin-insight`,
      options: {
        partnerId: `2137017`,
  
        // Include LinkedIn Insight in development.
        // Defaults to false meaning LinkedIn Insight will only be loaded in production.
        includeInDevelopment: false
      }
    },
    {
      resolve: `gatsby-plugin-twitter-pixel`,
      options: {
        pixelId: 'oev4x',
      },
    },
    `gatsby-plugin-portal`,
    // Must be placed at the end
    'gatsby-plugin-offline',
  ],
};
